# SimilarWeb Lite CSS / SCSS Style Guide

Other Style Guides:

  - [ES5](../../javascript/es5/)  
  - [Backbone](../../javascript/backbone/)  

## Table of Contents

  1. [Class naming conventions](#markdown-header-class-naming-conventions)
  1. [Property Order](#markdown-header-property-order)
  1. [Formatting](#markdown-header-formatting)
  1. [Comments](#markdown-header-comments)
  1. [Interacting with Javascript](#markdown-header-interacting-with-javascript)
  1. [Media Queries](#markdown-header-media-queries)


## Class naming conventions  
(*based off of the BEM methodology*)  
**We are using a Strict rule of only styling classes in CSS.**  
That means, absolutely no IDs, element tags, etc. Only classes. This solves several issues in CSS, most notably selector inheritance, and allows you to build very useful cascades of styles that are not redundant.

    :::scss
    /* Component */
    .componentName {}

    /* Modification to a component */
    .componentName {
      &--modifierName {}
    }

    /* Element within a Component */
    .componentName-elementName {}

    /* Element modifier */
    .componentName-element {
      &--modifierName {}
    }

    /* Component or element state change (usually via JS) */
     .componentName {
       &.is-stateOfComponent{}
     }
     .componentName-element {
       &.is-stateOfElement {}
     }

Regardless of how many levels the element is nested within the component, the class is always just the componentName-element (Never write: `.ComponentName-element1-element2`).

**[⬆ back to top](#markdown-header-table-of-contents)**

## Property Order
  * Property declarations
  Every property doesn't need to be in the same exact order every time, but we should at least follow these 2 rules
    1. Order most important properties (that heavily effect the display) first. (display, float, height, width, etc.)
    1. Order similar/ dependent properties together (font-style & font-weight, or border & outline)

  * `@include` declarations
Grouping `@includes` at the end makes it easier to read the entire selector.

        :::scss
        .btn-green {
          background: green;
          font-weight: bold;
          @include transition(background 0.5s ease);
          // ...
        }

  * Nested selectors
Nested selectors, if necessary, go last, and nothing goes after them. Add whitespace between your rule declarations and nested selectors, as well as between adjacent nested selectors. Apply the same guidelines as above to your nested selectors.

        :::scss
        .btn {
          background: green;
          font-weight: bold;
          @include transition(background 0.5s ease);

          .icon {
            margin-right: 10px;
          }
        }

**Do not nest selectors more than three levels deep!**

**[⬆ back to top](#markdown-header-table-of-contents)**

## Formatting

  * Use soft tabs (2 spaces) for indentation
  * Do not use ID selectors
  * Use single quotes `'`
  * When using multiple selectors in a rule declaration, give each selector its own line.
  * Put a space before the opening brace `{` in rule declarations
  * In properties, put a space after, but not before, the `:` character.
  * Put closing braces `}` of rule declarations on a new line
  * Put blank lines between rule declarations

        :::css
        /* Bad */
        .avatar{
            border-radius:50%;
            border:2px solid white; }
        .no, .nope, .not_good {
            // ...
        }
        #lol-no {
          // ...
        }


        /* Good */
        .avatar {
          border-radius: 50%;
          border: 2px solid white;
        }

        .one,
        .selector,
        .per-line {
          // ...
        }

  * Always define **font** with arguments variable, don't use quotes for arguments value

        :::scss
        // bad
        .selector {
          font-size: 14px;
          font-weight: 300;
        }

        // bad
        .selector {
          @include font($size: 'ms');
        }

        // good
        .selector {
          @include font($size: base, $weight: 300);
        }


**[⬆ back to top](#markdown-header-table-of-contents)**

## Comments

  * No magic numbers. Comment every number that is not ABSOLUTELY obvious why it is in the code

        :::css
        .my-class { margin-top: 87px; } // margin-top equals the calculated height of the header

  * Comment each major section and describe where it is used. Leave space to help indicate section starts ( can use // comments so that they don't appear in compiled css)

        :::css
        //*
        // Pricing section -  used on similarweb.com/pro and secure.similarweb.com/pricing
        //*
        #pricing { .. }

  * Comment every usage of a pseudo-element

        :::css
        &:after { // holds the site's thumbnail
          ...
        }

**[⬆ back to top](#markdown-header-table-of-contents)**

## Interacting with Javascript

Avoid binding to the same class in both your CSS and JavaScript. Conflating the two often leads to, at a minimum, time wasted during refactoring when a developer must cross-reference each class they are changing, and at its worst, developers being afraid to make changes for fear of breaking functionality.

There are two special class naming conventions for Elements that are changed via Javascript.

    :::css
    /* Elements who just need to be selected in Javsacript */
     .js-elementName

    /* Add a new class to an elements to modify its state */
     .is-newState

For example:
After a certain amount of scrolling the header becomes fixed. In order to select the header, we add a special Javascript class to it in the HTML `.js-header`. To add the class which changes the header's state to fixed position, add `.is-fixed`.
We should also try to avoid applying **ANY** styles directly in the JS. These are difficult to find, confusing to keep track of and not optimized. If it absolutely needs to be done to provide some sort of effect that cannot be created using CSS transitions or animations, Then the appropriate CSS file needs to be commented accordingly in order to let the developer know that there are additional styles outside of the stylesheet affecting the element.

**[⬆ back to top](#markdown-header-table-of-contents)**

## Media Queries
Media queries should be applied inline within the styles of a component, rather than creating separate files for each media query.  The idea is that each module will be defined only once and in a single place where every developer can find everything related to it. It also allows the modules to easily port to other projects and sites.

    :::scss
    .table {
      // some regular CSS here ...
      @include respondTo($tablet) { width: 100%; }
      @include respondTo($phone) { padding: 20px; }
    }

**[⬆ back to top](#markdown-header-table-of-contents)**
