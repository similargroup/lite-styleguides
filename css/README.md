# SimilarWeb Lite CSS Style Guide

* [SCSS](scss/)

If you want, you can use CSS coding style formatter [http://csscomb.com/](http://csscomb.com/)

**CSScomb** is a coding style formatter for CSS. You can easily write your own configuration to make your style sheets beautiful and consistent.
The main feature is sorting properties in a specific order.

To use it with Webstorm:

1. Global installation (for use as a command-line tool):

        :::bash
        npm install csscomb -g

2. Add new external tool in WebStorm:

    *File->Settings->Tools->External Tools->+*

    In the new window check next settings:  
    **Program**: C:\Users\ *<User name>* \AppData\Roaming\npm\csscomb.cmd  
    **Parameters**: $FilePath$  
    **Working directory**: $ProjectFileDir$  

    ![WebStorm Settings for csscomb](http://i.imgur.com/rjub9Oo.png)

3. All the settings you can configure in `.csscomb.json`, that should be in the root directory.
4. If you want to use the same configuration for CSS through all the projects, go to:
    *C:\Users\<User name>\AppData\Roaming\npm\node_modules\csscomb\config*
    and change the `csscomb.json`.