# SimilarWeb Lite Style Guides


## Table of Contents

  1. [JavaScript](javascript/)  
      1.1. [ES5](javascript/es5/)  
      1.2. [Backbone](javascript/backbone/)  
      1.3. [linters](javascript/linters/)
  1. [CSS](css/)  
      2.1. [SCSS](css/scss/)



![Style Guide](http://truuu.com/wp-content/uploads/2010/12/code_quality.jpg)