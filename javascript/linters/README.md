# .eslintrc

1. Install ESLint:

        :::bash
        npm install eslint --save-dev

2. Install the ESLint grunt integration:

        :::bash
        npm install grunt-eslint --save-dev

3. Install load-grunt-tasks:

        :::bash
        npm install load-grunt-tasks --save-dev

4. Install Backbone plugins:

        :::bash
        npm install eslint-plugin-backbone --save-dev


## How to start using ESLint

It was made a 'pre-commit' hook for git. It means that eslint will be triggered each time you'll make a commit.  
As '.git/' folder is not merged to master, you should add [this file][id-pre-commit] by your own to next path:

  <Path to your project>/similarWeb/similarweb/.git/hooks/

[id-pre-commit]: pre-commit  "pre-commit"