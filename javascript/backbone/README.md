# SimilarWeb Lite Backbone Style Guide

Other Style Guides:

  - [ES5](../es5/)  
  - [SCSS](../../css/scss)


## Table of Contents

  1. [Collections](#markdown-header-collections)
  1. [Models](#markdown-header-models)
  1. [Views](#markdown-header-views)
  1. [Events](#markdown-header-events)
  1. [Resources](#markdown-header-resources)


## Collections

  - All collections should declare model. eslint-plugin-backbone: [`collection-model`](https://github.com/ilyavolodin/eslint-plugin-backbone/blob/master/docs/rules/collection-model.md)

      > Declaring model type on a collection will allow you to pass raw objects into create, add and reset methods of that collection. This simplifies code and improves readability.

        :::js
        // bad
        Backbone.Collection.extend({
          initialize: function() {
            ...
          }
        });

        // good
        Backbone.Collection.extend({
          model: Book,
          initialize: function() {
            ...
          }
        });

  - Don't access directly to models property of collections. eslint-plugin-backbone: [`no-collection-models`](https://github.com/ilyavolodin/eslint-plugin-backbone/blob/master/docs/rules/no-collection-models.md)

      > Instead of accessing `models` collection directly from within views, use `get()`, `at()` or underscore functions. If you are looking for length of the collection, use `this.length` instead. If you want to modify collection use `this.add`, `this.remove`, `this.push` and `this.pop` methods instead.

        :::js
        // bad
        Backbone.Collection.extend({
          initialize: function() {
            _.first(this.models);
          }
        });

        // good
        Backbone.Collection.extend({
          initialize: function() {
            this.at(0);
          }
        });

**[⬆ back to top](#markdown-header-table-of-contents)**

## Models

  - Set `defaults` in the top of the model. eslint-plugin-backbone: [`defaults-on-top`](https://github.com/ilyavolodin/eslint-plugin-backbone/blob/master/docs/rules/defaults-on-top.md)

        :::js
        // bad
        Backbone.Model.extend({
            initialize: function() {
                ...
            },
            defaults: {}
        });

        // good
        Backbone.Model.extend({
            defaults: {},
            initialize: function() {
                ...
            }
        });

  - Don't access directly to attributes collection inside models. eslint-plugin-backbone: [`no-model-attributes`](https://github.com/ilyavolodin/eslint-plugin-backbone/blob/master/docs/rules/no-model-attributes.md)

      > Instead of accessing `attributes` collection directly from within models, use `get()` or `set()` functions. Backbone setters do more then just assign value, they also keep track of when model has been last modified. If you work with `attributes` collection directly, `changed` and `changedAttributes` will not be updated.

        :::js
        // bad
        Backbone.Model.extend({
          initialize: function() {
              _.first(this.attributes);
          }
        });

        // good
        Backbone.Model.extend({
          initialize: function() {
              this.set('test', true);
          }
        });

**[⬆ back to top](#markdown-header-table-of-contents)**

## Views

  - `constuctor` should always be present in your View. And it should be the first thing registered in the View. There is only one exclusion. If you use `tagName` or `className` properties in your View, you can declare them before `constructor`.
  The name of function (Identifier) should be the same as the name of View.
  When a function has a corresponding identifier, debuggers show that identifier as a function name, when inspecting call stack.

        :::js
        PageModule.Views.SpecialName = Backbone.View.extend({
          constructor: function SpecialName() {
            Backbone.View.prototype.constructor.apply(this, arguments);
          },
          // ...
        });

  - `events` should be next after `constructor`. eslint-plugin-backbone: [`events-on-top`](https://github.com/ilyavolodin/eslint-plugin-backbone/blob/master/docs/rules/events-on-top.md)
  - Set `defaults` after `events`. You can divide `defaults` into several groups if there is need:

        :::js
        Backbone.View.extend({
          constructor: function Identifier() {},

          // defaults start
          generalClasses: {
            inputs: '.js-textInput',
            inputWrappers: '.js-wrapTextInput',
            // ...
            stateSelect: '.js-states-values'
          },
          fields: {
            country: '.js-country',
            state: '.js-state',
            // ...
            termsError: '.js-terms-error'
          },
          elements: {
            $form: this.$('.js-purchaseForm'),
            $inputs: this.$('.js-textInput'),
            // ...
            $zip: this.$('.js-zip')
          }
          // defaults end

        });

  - Set `initialize` after `defaults`.

  - The default View structure:

        :::js
        PageModule.Views.SpecialName = Backbone.View.extend({
          className: '',
          constructor: function SpecialName() {
            Backbone.View.prototype.constructor.apply(this, arguments);
          },
          events: {},
          defaults: {},
          initialize: function() {
              ...
          }
        });

  - Don't set changed attribute of the model in views.  eslint-plugin-backbone: [`no-changed-set`](https://github.com/ilyavolodin/eslint-plugin-backbone/blob/master/docs/rules/no-changed-set.md)

      > Changed attribute is automatically computed by Backbone and modified when any property of the model has been updated. Manually changing it can lead to problems.

        :::js
        // bad
        Backbone.View.extend({
          render: function() {
            this.model.changed = false;
          }
        });

  - Don't assign el or $el inside views. eslint-plugin-backbone: [`no-el-assign`](https://github.com/ilyavolodin/eslint-plugin-backbone/blob/master/docs/rules/no-el-assign.md)

      > Setting `this.el` or `this.$el` can cause issues, since Backbone will not automatically bind events to the element. Use `setElement` function instead which will cache a reference to an element and bind all events.

        :::js
        // bad
        Backbone.View.extend({
          initialize: function() {
            this.$el = $(".test");
          }
        });

        // bad
        Backbone.View.extend({
          initialize: function() {
            this.el = $(".test")[0];
          }
        });

        // good
        Backbone.View.extend({
          initialize: function() {
            this.setElement($(".test"));
          }
        });

  - Don't use $ in the views. eslint-plugin-backbone: [`no-native-jquery`](https://github.com/ilyavolodin/eslint-plugin-backbone/blob/master/docs/rules/no-native-jquery.md)

      > When operating on DOM inside views, you should use `this.$` instead of native jQuery. This will limit the scope to the elements to the children of view's element. Views should not operate on DOM outside of it's area of responsibilities. This will lead to confusion and will make a large code base hard to debug.

        :::js
        // bad
        Backbone.View.extend({
          render: function() {
            var a = $(".test").offset();
          }
        });

        // good
        Backbone.View.extend({
          render: function() {
            var a = this.$(".test").offset();
          }
        });

  - Don't access directly to collection's models property. eslint-plugin-backbone: [`no-view-collection-models`](https://github.com/ilyavolodin/eslint-plugin-backbone/blob/master/docs/rules/no-view-collection-models.md)

    > Instead of accessing `models` collection directly from within views, use `get()`, `at()` or underscore functions.

        :::js
        // bad
        Backbone.View.extend({
          render: function() {
            alert(this.model.models.length);
          }
        })

        // good
        Backbone.View.extend({
          render: function() {
            alert(this.collection.at(0));
          }
        });

  - Don't access to model's attributes collection. eslint-plugin-backbone: [`no-view-model-attributes`](https://github.com/ilyavolodin/eslint-plugin-backbone/blob/master/docs/rules/no-view-model-attributes.md)

    > Instead of accessing `attributes` collection directly from within views, use `get()` or `set()` functions. Backbone setters do more then just assign value, they also keep track of when model has been last modified. If you work with `attributes` collection directly, `changed` and `changedAttributes` will not be updated.

        :::js
        // bad
        Backbone.View.extend({
          render: function() {
            alert(this.model.attributes[0]);
          }
        });

        // good
        Backbone.View.extend({
          render: function() {
            alert(this.model.get("test"));
          }
        });

  - Don't use on/off bindings. eslint-plugin-backbone: [`no-view-onoff-binding`](https://github.com/ilyavolodin/eslint-plugin-backbone/blob/master/docs/rules/no-view-onoff-binding.md)

    > Instead of using `on` and `off` methods to subscribe to model/collection events, use `listenTo` and `stopListening` methods. If you remove a view that uses `on` binding to the model events, view is not completely removed from the memory, because model still has reference to event handler function that lives inside the view. This can be avoided by using `off` before removing a view. Or by using `listenTo` function.

        :::js
        // bad
        Backbone.View.extend({
          initalize: function() {
            this.model.on("change", this.render);
          }
        });

        // good
        Backbone.View.extend({
          initialize: function() {
            this.listenTo(this.model, "change", this.render);
          }
        });


**[⬆ back to top](#markdown-header-table-of-contents)**

## Events

  - Be sure that scope is passed into event handlers. eslint-plugin-backbone: [`event-scope`](https://github.com/ilyavolodin/eslint-plugin-backbone/blob/master/docs/rules/event-scope.md)

      > When binding event handlers for Backbone events such as `add`, `remove`, etc. it's a good idea to pass a parameter that will switch context. While sometimes it's not necessary, in most cases it will make your code more consistent and easier to understand. This rule applies to `on` and `once` operators.


        ::::js
        // bad
        Backbone.Model.extend({
          initialize: function() {
            this.on('change', this.modelChanged);
          }
        });

        Backbone.Model.extend({
           nitialize: function() {
            this.on({'change': this.modelChanged});
          }
        });

        // good
        Backbone.Model.extend({
          initialize: function() {
            this.on('change', this.modelChanged, this);
          }
        });

        Backbone.Model.extend({
          initialize: function() {
            this.on({'change': this.modelChanged}, this);
          }
        });

  - Don't use silent option in functions that cause events. eslint-plugin-backbone: [`no-silent`](https://github.com/ilyavolodin/eslint-plugin-backbone/blob/master/docs/rules/no-silent.md)

    > Per Backbone documentation, passing `silent` flag is rarely if ever a good idea. A better way is to have your event handler determine if the event needs to be accepted on. This rule applies to the following commands: `set`, `unset`, `reset`, `clear`, `remove`, `add`, `push`, `unshift`, `shift`, `sort`, `create`

        :::js
        // bad
        Backbone.Model.extend({
          intialize: function() {
            this.set('test', 'test', {silent:true});
          }
        });

        // good
        Backbone.Model.extend({
          intialize: function() {
            this.set('test', 'test');
          }
        });

  - If method doesn't return a value explicitly (as getter methods do), and is not an event handler, then it should return `this`.
    Render function is a particular case of this rule. eslint-plugin-backbone: [`render-return`](https://github.com/ilyavolodin/eslint-plugin-backbone/blob/master/docs/rules/render-return.md)

        :::js
        // bad
        Backbone.View.extend({
          render: function() {
            ...
          }
        });

        // good
        Backbone.View.extend({
          render: function() {
            ...
            return this;
          }
        });

**[⬆ back to top](#markdown-header-table-of-contents)**

## Resources

**Tools**

  - Code Style Linters
    + [ESHint](http://eslint.org/) - [SimilarWeb Style .eslintrc](https://bitbucket.org/similargroup/lite-styleguides/src/abadba82b91e5a232b47aea957acc9c7bef305a5/javascript/linters/.eslintrc)

**Further Reading**

  - [BackboneJS Documentation](http://backbonejs.org/)

**[⬆ back to top](#markdown-header-table-of-contents)**